#!/usr/bin/awk -f
BEGIN {
	start = 0;
	total = 0;
	count = 0;
}

/cature_started/ {
	start = 1;
}

/do_cleanup/ {
	start = 0;
}

{
	if(start) {
		print $1
		total += 0.0 + $1
		count++
	}
}

END {
	average = total / count
	print "average = " average
	print "fps     = " 1.0 / average	
}

