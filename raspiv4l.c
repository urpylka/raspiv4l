/*
Copyright 2016 Ben Farrell 

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <termios.h>
#include <getopt.h>        /* getopt_long() */

#include <fcntl.h>        /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <time.h>
#include <signal.h>

#include <linux/videodev2.h>
#include <libswscale/swscale.h>
#include <libavcodec/avcodec.h>

#define OUTPUT_IS_X264_RPI      1
#define OUTPUT_IS_SDL 2
#define OUTPUT_IS_NULL     3

#ifndef OUTPUT_IS
#define OUTPUT_IS  OUTPUT_IS_X264_RPI
//#define OUTPUT_IS  OUTPUT_IS_NULL
#endif

#if(OUTPUT_IS==OUTPUT_IS_SDL)
#include <SDL/SDL.h>
#include <SDL/SDL_thread.h>
static SDL_Surface *screen = NULL;
static SDL_Overlay *bmp = NULL;
#endif

#if(OUTPUT_IS==OUTPUT_IS_X264_RPI)
#include <bcm_host.h>
#include <ilclient.h>

COMPONENT_T *video_encode = NULL;
OMX_BUFFERHEADERTYPE *buf;

COMPONENT_T *list[5];
ILCLIENT_T *client;
#endif

#define CLEAR(x) memset(&(x), 0, sizeof(x))

static int frame_rate = 0;

struct buffer {
    void *start;
    size_t length;
};

static char *dev_name;
static int fd = -1;
struct buffer *buffers;
static unsigned int n_buffers;
static int out_buf = 0;
static int frame_count = 0;
int omx_input_port = 200;
static int print_frame_time = 0;
static int deinterlace = 1; //-2;

struct capture_params {
    int width;
    int height;
    int pixformat;
    enum v4l2_field field;
};

struct encoder_params {
    int width;
    int height;
    int width32;
    int height16;
    int pixformat;
};

struct capture_params input = { 0, 0, 0 };
struct encoder_params output = { 0, 0, 0, 0, PIX_FMT_YUV420P };

static struct SwsContext *img_convert_ctx = NULL;


/* PROTOTYPES */
static void x264_open();
static void x264_encode();
static void x264_close();

static void get_capture_rate(struct v4l2_format *fmt);
static void init_mmap(void);
static int read_frame(void);
static void process_image(const void *p, int size);

/* UTILITIES */


void changemode(int dir)
{
  static struct termios oldt, newt;

  if ( dir == 1 )
  {
    tcgetattr( STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);
  }
  else
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
}

int kbhit (void)
{
  struct timeval tv;
  fd_set rdfs;

  tv.tv_sec = 0;
  tv.tv_usec = 0;

  FD_ZERO(&rdfs);
  FD_SET (STDIN_FILENO, &rdfs);

  select(STDIN_FILENO+1, &rdfs, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &rdfs);

}

static void myexit(const char *s)
{
    if (EINVAL == errno)
    {
        // exit with no errno
        fprintf(stderr,  "%s error\n", s);
        exit(EXIT_FAILURE);
    }
    else // exit with errno
    {
        fprintf(stderr,  "%s error: %d, %s\n", s, errno, strerror(errno));
        exit(EXIT_FAILURE);
    }
}

static int xioctl(int fh, int request, void *arg)
{
    int r;

    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

#if(OUTPUT_IS==OUTPUT_IS_X264_RPI)
static void print_def(OMX_PARAM_PORTDEFINITIONTYPE def)
{
    fprintf(stderr, "Port %lu: %s %lu/%lu %lu %lu %s,%s,%s %lux%lu %lux%lu @%lu %u\n",
           (long unsigned)def.nPortIndex,
           def.eDir == OMX_DirInput ? "in" : "out",
           (long unsigned)def.nBufferCountActual,
           (long unsigned)def.nBufferCountMin,
           (long unsigned)def.nBufferSize,
           (long unsigned)def.nBufferAlignment,
           def.bEnabled ? "enabled" : "disabled",
           def.bPopulated ? "populated" : "not pop.",
           def.bBuffersContiguous ? "contig." : "not cont.",
           (long unsigned)def.format.video.nFrameWidth,
           (long unsigned)def.format.video.nFrameHeight,
           (long unsigned)def.format.video.nStride,
           (long unsigned)def.format.video.nSliceHeight,
           (long unsigned)def.format.video.xFramerate, (unsigned)def.format.video.eColorFormat);
}
#endif

static void open_device(void)
{
    struct stat st;

    if (-1 == stat(dev_name, &st)) {
        fprintf(stderr,  "Cannot identify '%s': %d, %s\n", dev_name, errno,
             strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (!S_ISCHR(st.st_mode)) {
        fprintf(stderr,  "%s is no device\n", dev_name);
        exit(EXIT_FAILURE);
    }

    fd = open(dev_name, O_RDWR /* required */  | O_NONBLOCK, 0);

    if (-1 == fd) {
        fprintf(stderr,  "Cannot open '%s': %d, %s\n", dev_name, errno,
             strerror(errno));
        exit(EXIT_FAILURE);
    }
}

static void init_device(void)
{
    struct v4l2_capability cap;
    struct v4l2_format fmt;

    if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
        myexit("VIDIOC_QUERYCAP");
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        myexit("Not a capture device");
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        myexit("Steaming I/O not supported");
    }

    /* get current format */
    CLEAR(fmt);
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == xioctl(fd, VIDIOC_G_FMT, &fmt))
        myexit("VIDIOC_G_FMT");

    fmt.fmt.pix.field = V4L2_FIELD_NONE;

    if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt))
        myexit("VIDIOC_S_FMT");

    fprintf(stderr, "V4L cpature format, w=%d, h=%d, format=%c%c%c%c, field=%d\n",
        fmt.fmt.pix.width, fmt.fmt.pix.height,
        fmt.fmt.pix.pixelformat, fmt.fmt.pix.pixelformat >> 8,
        fmt.fmt.pix.pixelformat >> 16, fmt.fmt.pix.pixelformat >> 24, fmt.fmt.pix.field);

    get_capture_rate(&fmt);

    init_mmap();

    /* setup sw scaler for converting pixel format to YUV420P */
    input.width = fmt.fmt.pix.width;
    input.height = fmt.fmt.pix.height;
    input.field = fmt.fmt.pix.field;

    output.width   =  input.width;
    output.height   = input.height;
    output.width32  = (input.width+31)&~31;
    output.height16 = (output.height+15)&~15;

    fprintf(stderr, "OMX width32 = %d height16 = %d\n", output.width32,  output.height16);

    switch (fmt.fmt.pix.pixelformat) {
    case V4L2_PIX_FMT_YUYV:
        input.pixformat = PIX_FMT_YUYV422;
        break;
    case V4L2_PIX_FMT_UYVY:
        input.pixformat = PIX_FMT_UYVY422;
        break;
    case V4L2_PIX_FMT_YUV411P:
        input.pixformat = PIX_FMT_YUV411P;
        break;
    case V4L2_PIX_FMT_SBGGR8: // 'BA81'
        myexit("Unspported pixel format");
        break;
    default:
        myexit("!ERROR! unknown pixel format");
    }

    img_convert_ctx = sws_getContext(input.width, input.height, input.pixformat,
                     output.width, output.height, output.pixformat,
                     SWS_PRINT_INFO | SWS_FAST_BILINEAR, NULL, NULL, NULL);

    if (img_convert_ctx == NULL) {
        myexit("SWS_GETCONTEXT");
    }
}

static void get_capture_rate(struct v4l2_format *fmt)
{
    struct v4l2_frmivalenum temp;
    float stepval = 0;
    float cval;
    struct v4l2_streamparm sparm;

    CLEAR(temp);
    temp.pixel_format = fmt->fmt.pix.pixelformat;
    temp.width = fmt->fmt.pix.width;
    temp.height = fmt->fmt.pix.height;

    xioctl(fd, VIDIOC_ENUM_FRAMEINTERVALS, &temp);

    fprintf(stderr, "V4L Supported rates(fps): ");

    if (temp.type == V4L2_FRMIVAL_TYPE_DISCRETE) {
        while (xioctl(fd, VIDIOC_ENUM_FRAMEINTERVALS, &temp) != -1) {
            fprintf(stderr, "%f ", (float) temp.discrete.denominator/temp.discrete.numerator);
            temp.index += 1;
        }
    }

    if (temp.type == V4L2_FRMIVAL_TYPE_CONTINUOUS) {
        stepval = 1;
    }
    if (temp.type == V4L2_FRMIVAL_TYPE_STEPWISE || temp.type == V4L2_FRMIVAL_TYPE_CONTINUOUS) {
        float minval = (float)temp.stepwise.min.numerator/temp.stepwise.min.denominator;
        float maxval = (float)temp.stepwise.max.numerator/temp.stepwise.max.denominator;
        if (stepval == 0) {
            stepval = (float)temp.stepwise.step.numerator/temp.stepwise.step.denominator;
        }
        for (cval = minval; cval <= maxval; cval += stepval) {
            fprintf(stderr, "%f ", (float) 1/cval);
        }
    }
    fprintf(stderr, "\n");

    CLEAR(sparm);
    sparm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    xioctl(fd, VIDIOC_G_PARM, &sparm);

    cval = (float) sparm.parm.capture.timeperframe.denominator / sparm.parm.capture.timeperframe.numerator;

    fprintf(stderr, "v4L current rate %d/%d = %f fps\n",
        sparm.parm.capture.timeperframe.numerator,
        sparm.parm.capture.timeperframe.denominator,
        cval);

    if(0 == frame_rate){
        frame_rate = (int) cval;
    }

    errno = EINVAL; // todo one of the ioctl is failing ..
}

static void init_mmap(void)
{
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        myexit("VIDIOC_REQBUFS");
    }

    if (req.count < 2) {
        myexit("Insufficient buffers memory");
    }

    buffers = calloc(req.count, sizeof(*buffers));

    if (!buffers) {
        myexit("Out of memory");
    }

    for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
        struct v4l2_buffer buf;

        CLEAR(buf);

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = n_buffers;

        if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
            myexit("VIDIOC_QUERYBUF");

        buffers[n_buffers].length = buf.length;
        buffers[n_buffers].start = mmap(NULL /* start anywhere */ ,
                        buf.length, PROT_READ | PROT_WRITE /* required */ ,
                        MAP_SHARED /* recommended */ ,
                        fd, buf.m.offset);

        if (MAP_FAILED == buffers[n_buffers].start)
            myexit("mmap");
    }
}

void x264_open()
{
#if(OUTPUT_IS==OUTPUT_IS_SDL)
    if (SDL_Init(SDL_INIT_VIDEO)) {
        fprintf(stderr,  "Could not initialize SDL - %s\n", SDL_GetError());
        exit(1);
    }

    screen = SDL_SetVideoMode(output.width, output.height, 0, 0);
    if (!screen) {
        myexit("SDL: could not set video mode - exiting\n");
    }

    bmp = SDL_CreateYUVOverlay(output.width, output.height, SDL_YV12_OVERLAY, screen);
#endif

#if(OUTPUT_IS==OUTPUT_IS_X264_RPI)
    OMX_PARAM_PORTDEFINITIONTYPE def;
    OMX_ERRORTYPE r = 0;

    bcm_host_init();

    memset(list, 0, sizeof(list));

    if ((client = ilclient_init()) == NULL) {
        return;
    }

    if (OMX_Init() != OMX_ErrorNone) {
        fprintf(stderr,"omx_init failed\n");
        ilclient_destroy(client);
        return;
    }

    if(omx_input_port == 200)
    {
        // create video_encode
        r = ilclient_create_component(client, &video_encode, "video_encode",
                          ILCLIENT_DISABLE_ALL_PORTS |
                          ILCLIENT_ENABLE_INPUT_BUFFERS |
                          ILCLIENT_ENABLE_OUTPUT_BUFFERS);
    }
    else if(omx_input_port == 90)
    {
        r = ilclient_create_component(client, &video_encode, "video_render",
                          ILCLIENT_DISABLE_ALL_PORTS |
                          ILCLIENT_ENABLE_INPUT_BUFFERS);
    }

    if (r != 0) {
        fprintf(stderr,"ilclient_create_component(), input_port %d failed with %x!\n",omx_input_port ,r);
        exit(1);
    }
    list[0] = video_encode;

    memset(&def, 0, sizeof(OMX_PARAM_PORTDEFINITIONTYPE));
    def.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
    def.nVersion.nVersion = OMX_VERSION;
    def.nPortIndex = omx_input_port;

    if (OMX_GetParameter(ILC_GET_HANDLE(video_encode), OMX_IndexParamPortDefinition, &def) != OMX_ErrorNone) {
        fprintf(stderr, "OMX_GetParameter() for video_encode port %d failed!\n", omx_input_port);
        exit(1);
    }

    print_def(def);

    def.format.video.nFrameWidth = output.width;
    def.format.video.nFrameHeight = output.height;
    def.format.video.xFramerate = frame_rate << 16;
    def.format.video.nStride = output.width32;
    def.format.video.nSliceHeight = output.height16;
    def.format.video.eColorFormat = OMX_COLOR_FormatYUV420PackedPlanar;
    def.format.video.nBitrate = 64000;

    print_def(def);

    r = OMX_SetParameter(ILC_GET_HANDLE(video_encode), OMX_IndexParamPortDefinition, &def);
    if (r != OMX_ErrorNone) {
        fprintf(stderr, "OMX_SetParameter() for video_encode port %d failed with %x!\n", omx_input_port, r);
        exit(1);
    }

    if(omx_input_port == 200)
    {
        OMX_VIDEO_PARAM_PORTFORMATTYPE format;
        memset(&format, 0, sizeof(OMX_VIDEO_PARAM_PORTFORMATTYPE));

        format.nSize = sizeof(OMX_VIDEO_PARAM_PORTFORMATTYPE);
        format.nVersion.nVersion = OMX_VERSION;
        format.nPortIndex = 201;
        format.eCompressionFormat = OMX_VIDEO_CodingAVC;

        fprintf(stderr, "OMX_SetParameter for video_encode:201...\n");
        if(OMX_ErrorNone != OMX_SetParameter(ILC_GET_HANDLE(video_encode), OMX_IndexParamVideoPortFormat, &format)) {
            myexit("OMX_SetParameter(OMX_IndexParamVideoPortFormat) for video_encode port 201 failed with %x!\n");
        }

        memset(&def, 0, sizeof(OMX_PARAM_PORTDEFINITIONTYPE));
        def.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
        def.nVersion.nVersion = OMX_VERSION;
        def.nPortIndex = 201;

        if (OMX_ErrorNone != OMX_GetParameter(ILC_GET_HANDLE(video_encode), OMX_IndexParamPortDefinition, &def)) {
            myexit("OMX_GetParameter(OMX_IndexParamPortDefinition) for video_encode port 201 failed!\n");
        }

        def.format.video.xFramerate = frame_rate << 16;
        def.format.video.nBitrate = 64000;
        print_def(def);

        if(OMX_ErrorNone != OMX_SetParameter(ILC_GET_HANDLE(video_encode), OMX_IndexParamPortDefinition, &def)) {
            myexit("OMX_SetParameter(OMX_IndexParamPortDefinition) for video_encode port 201 failed!\n");
        }
#if 0
        {
            OMX_VIDEO_CONFIG_AVCINTRAPERIOD avc_int;
            memset( &avc_int, 0, sizeof(OMX_VIDEO_CONFIG_AVCINTRAPERIOD));

            avc_int.nVersion.nVersion = OMX_VERSION;
            avc_int.nPortIndex = 201;

            r = OMX_GetConfig(ILC_GET_HANDLE(video_encode), OMX_IndexConfigVideoAVCIntraPeriod, (OMX_PTR)&avc_int );
            if(r != OMX_ErrorNone) {
                fprintf(stderr,"OMX_GetConfig(OMX_IndexConfigVideoAVCIntraPeriod) failed with %x!\n", r);
                exit(1);
            }

            //    avc_int.nIDRPeriod = 25;
            avc_int.nPFrames = 29;

            r = OMX_SetConfig(ILC_GET_HANDLE(video_encode), OMX_IndexConfigVideoAVCIntraPeriod, (OMX_PTR)&avc_int );
            if(r != OMX_ErrorNone) {
                fprintf(stderr,"OMX_GetConfig(OMX_IndexConfigVideoAVCIntraPeriod) failed with %x!\n", r);
                exit(1);
            }
        }
#endif
    }


    fprintf(stderr, "encode to idle...\n");
    if (ilclient_change_component_state(video_encode, OMX_StateIdle) == -1) {
        fprintf(stderr,"ilclient_change_component_state(video_encode, OMX_StateIdle) failed");
    }

    fprintf(stderr, "enabling port buffers for %d...\n", omx_input_port);
    if (ilclient_enable_port_buffers(video_encode, omx_input_port, NULL, NULL, NULL) != 0) {
        fprintf(stderr, "enabling port buffers for %d failed!\n", omx_input_port);
        exit(1);
    }

    if(omx_input_port == 200)
    {
        fprintf(stderr, "enabling port buffers for 201...\n");
        if (ilclient_enable_port_buffers(video_encode, 201, NULL, NULL, NULL) != 0) {
            fprintf(stderr, "enabling port buffers for 201 failed!\n");
            exit(1);
        }
    }

    fprintf(stderr, "encode to executing...\n");
    ilclient_change_component_state(video_encode, OMX_StateExecuting);
#endif
}

static void start_capturing(void)
{
    unsigned int i;
    enum v4l2_buf_type type;

    for (i = 0; i < n_buffers; ++i) {
        struct v4l2_buffer buf;

        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
            myexit("VIDIOC_QBUF");
    }

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
        myexit("VIDIOC_STREAMON");


    fprintf(stderr, "cature_started..\n");
}

void timespec_print_diff(struct timespec start)
{
     struct timespec end;
     struct timespec diff;

     //clock_gettime(CLOCK_MONOTONIC, &end);

     if ( (end.tv_nsec - start.tv_nsec) < 0) {
         diff.tv_sec = end.tv_sec-start.tv_sec - 1;
         diff.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
     } else {
         diff.tv_sec = end.tv_sec - start.tv_sec;
         diff.tv_nsec = end.tv_nsec - start.tv_nsec;
     }

     fprintf(stderr, "%d.%09ld\n", (int) diff.tv_sec, diff.tv_nsec);
}

static void mainloop(void)
{
    int count = (frame_count == 0) ? 1 : frame_count;

    changemode(1);

    while(count > 0) {

            struct timespec ts_start;

        //if(print_frame_time) clock_gettime(CLOCK_MONOTONIC, &ts_start);

        for (;;) {
            fd_set fds;
            struct timeval tv;
            int r;


            FD_ZERO(&fds);
            FD_SET(fd, &fds);

            /* Timeout. */
            tv.tv_sec = 20;
            tv.tv_usec = 0;

            r = select(fd + 1, &fds, NULL, NULL, &tv);

            if(kbhit())
            {
                char ch = getchar();
                if(ch=='i')
                {
                    deinterlace = ~deinterlace;
                }
            }

            if (-1 == r) {
                if (EINTR == errno)
                    continue;

                myexit("select");
            }

            if (0 == r) {
                myexit("select timeout\n");
            }

            if (read_frame())
                break;
            /* EAGAIN - continue select loop. */
        }

        if(frame_count != 0) count--;

        if(print_frame_time) timespec_print_diff(ts_start);
    }
}

static int read_frame(void)
{
    struct v4l2_buffer buf;

    CLEAR(buf);

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
        switch (errno) {
        case EAGAIN:
            return 0;

        case EIO:
            /* Could ignore EIO, see spec. */

            /* fall through */

        default:
            myexit("VIDIOC_DQBUF");
        }
    }

    assert(buf.index < n_buffers);

    process_image(buffers[buf.index].start, buf.bytesused);

    if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
        myexit("VIDIOC_QBUF");

    return 1;
}

static inline void linearBlend(unsigned char *src, int width, int height)
{
  unsigned int x, y;

  for(y = 1; y < height; y++)
  {
      for(x = 0; x < width; x++)
    {
        int newval = src[(y*width) + x] + src[((y-1)*width) + x];
        src[((y-1)*width) + x] = newval >> 1;
    }
  }
}

static inline void linearBlendWord(unsigned int value1, unsigned int *value2)
{
    *value2 =
        (0x000000ff & (((0x000000ff & value1) + (0x000000ff & *value2)) >> 1)) |
         (0x0000ff00 & (((0x0000ff00 & value1) + (0x0000ff00 & *value2)) >> 1)) |
        (0x00ff0000 & (((0x00ff0000 & value1) + (0x00ff0000 & *value2)) >> 1)) |
        (0xff000000 & (((0xff000000 &  value1)>>1) + ((0xff000000 & *value2)>>1) ));
}

static inline void linearBlend3(unsigned int *src, int width, int height)
{
    unsigned int x;
    width >>= 2;
    unsigned int count = height * width;
    unsigned int *nextline = src + width;

    for(x = 1; x < count; x+=8)
    {
        linearBlendWord(*nextline++, src++);
        linearBlendWord(*nextline++, src++);
        linearBlendWord(*nextline++, src++);
        linearBlendWord(*nextline++, src++);
        linearBlendWord(*nextline++, src++);
        linearBlendWord(*nextline++, src++);
        linearBlendWord(*nextline++, src++);
        linearBlendWord(*nextline++, src++);
    }
}


static void process_image(const void *p, int size)
{
    AVFrame *input_frame, *output_frame;

    /* populate input frame */
    input_frame = avcodec_alloc_frame();
    avpicture_fill((AVPicture *) input_frame,
               (uint8_t *) p, input.pixformat, input.width, input.height);

    /* populate output frame */
    output_frame = avcodec_alloc_frame();

#if(OUTPUT_IS==OUTPUT_IS_SDL)
    SDL_LockYUVOverlay(bmp);

    output_frame->data[0] = bmp->pixels[0];
    output_frame->data[1] = bmp->pixels[2];
    output_frame->data[2] = bmp->pixels[1];

    output_frame->linesize[0] = bmp->pitches[0];
    output_frame->linesize[1] = bmp->pitches[2];
    output_frame->linesize[2] = bmp->pitches[1];
#endif

#if(OUTPUT_IS==OUTPUT_IS_X264_RPI)
    buf = ilclient_get_input_buffer(video_encode, omx_input_port, 1);;
    if (buf == NULL) {
        fprintf(stderr,"Doh, no buffers for me!\n");
    } else {
        avpicture_fill((AVPicture *) output_frame,
                   (uint8_t *) buf->pBuffer, output.pixformat, output.width32, output.height16);
    }
#endif

#if(OUTPUT_IS==OUTPUT_IS_NULL)
    {
        int size = avpicture_get_size(output.pixformat, output.width32, output.height16);
        uint8_t *image = malloc(size);

        avpicture_fill((AVPicture *) output_frame,
                   (uint8_t *) image, output.pixformat, output.width32, output.height16);
    }
#endif


//    fprintf(stderr, "process_image: got v4l frame\n");

    /* Do the conversion */
    sws_scale(img_convert_ctx,
          (const uint8_t ** const) input_frame->data,
          input_frame->linesize,
          0, input.height, output_frame->data, output_frame->linesize);

    if((input.field == V4L2_FIELD_INTERLACED) && (deinterlace == 1))
    {
        /* avpicture_deinterlace((AVPicture *) input_frame, (const AVPicture *) input_frame,
                    input.pixformat, input.width, input.height); */

        linearBlend3(output_frame->data[0], output.width32, output.height16);
    }

    /* encode frame */
    x264_encode();

#if(OUTPUT_IS==OUTPUT_IS_NULL)
    free(output_frame->data[0]);
#endif
    av_free(input_frame);
    av_free(output_frame);

}

static long long time_stamp = 0;

static void x264_encode()
{
#if(OUTPUT_IS==OUTPUT_IS_SDL)
    SDL_Rect rect;

    SDL_UnlockYUVOverlay(bmp);

    rect.x = 0;
    rect.y = 0;
    rect.w = output.width;
    rect.h = output.height;
    SDL_DisplayYUVOverlay(bmp, &rect);
#endif

#if(OUTPUT_IS==OUTPUT_IS_X264_RPI)

    buf->nFilledLen = avpicture_get_size(output.pixformat, output.width32, output.height16);

#ifdef OMX_SKIP64BIT
    buf->nTimeStamp.nLowPart = (OMX_U32)  time_stamp;
    buf->nTimeStamp.nHighPart = (OMX_U32) (time_stamp>>32);
#else
    buf.nTimeStamp = time_stamp;
#endif

    time_stamp += (OMX_TICKS_PER_SECOND/frame_rate);


    if (OMX_EmptyThisBuffer(ILC_GET_HANDLE(video_encode), buf) != OMX_ErrorNone) {
        fprintf(stderr,"Error emptying buffer!\n");
    }


    if(omx_input_port == 200)
    {
        OMX_BUFFERHEADERTYPE *out;
        OMX_ERRORTYPE r;

        out = ilclient_get_output_buffer(video_encode, 201, 1);

        do {
            r = OMX_FillThisBuffer(ILC_GET_HANDLE(video_encode), out);
            if (r != OMX_ErrorNone) {
                fprintf(stderr,"Error filling buffer: %x\n", r);
            }

            if (out_buf) {
                fwrite(out->pBuffer, out->nFilledLen, 1, stdout);
                fflush(stdout);
            }

            out->nFilledLen = 0;
        } while ( !(out->nFlags & OMX_BUFFERFLAG_ENDOFFRAME) );
    }
#endif
}

static void close_device(void)
{
    unsigned int i;
    enum v4l2_buf_type type;

// stop()
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(fd, VIDIOC_STREAMOFF, &type))
        fprintf(stderr, "VIDIOC_STREAMOFF returned -1\n");

// uinit()
    for (i = 0; i < n_buffers; ++i)
        if (-1 == munmap(buffers[i].start, buffers[i].length))
            fprintf(stderr, "munmap(%d) returned -1", i);

    free(buffers);

// close()
    if (-1 == close(fd))
        fprintf(stderr, "close returned -1\n");

    fd = -1;

// free software scaler
    sws_freeContext(img_convert_ctx);
}

void x264_close()
{
#if(OUTPUT_IS==OUTPUT_IS_SDL)
    SDL_Quit();
#endif

#if(OUTPUT_IS==OUTPUT_IS_X264_RPI)

    if(video_encode)
    {
        fprintf(stderr,"disabling port buffers for %d and 201...\n", omx_input_port);
        ilclient_disable_port_buffers(video_encode, omx_input_port, NULL, NULL, NULL);

        if(omx_input_port == 200)
        {
            ilclient_disable_port_buffers(video_encode, 201, NULL, NULL, NULL);
        }
    }

    ilclient_state_transition(list, OMX_StateIdle);
    ilclient_state_transition(list, OMX_StateLoaded);

    ilclient_cleanup_components(list);

    OMX_Deinit();

    ilclient_destroy(client);
#endif
}

static void usage(FILE * fp, int argc, char **argv)
{
    fprintf(fp,
         "Usage: %s [options]\n\n"
         "Options:\n"
         "-d | --device name   Video device name [%s]\n"
         "-h | --help          Print this message\n"
         "-o | --output        Outputs stream to stdout\n"
         "-c | --count         Number of frames to grab [%i]. 0 = run forever\n"
         "-m | --monitor       Enable monitor mode (disables x264 encoded output)\n"
         "-t | --timings       Prints frame timings to stderr\n"
         "-f | --framerate     Override detected frame-rate from V4L [25]\n"
         "", argv[0], dev_name, frame_count);
}

static const char short_options[] = "d:hoc:mtf:";

static const struct option long_options[] = {
    {"device", required_argument, NULL, 'd'},
    {"help", no_argument, NULL, 'h'},
    {"output", no_argument, NULL, 'o'},
    {"count", required_argument, NULL, 'c'},
    {"monitor", no_argument, NULL, 'm'},
    {"timings", no_argument, NULL, 't'},
    {"framerate", required_argument, NULL, 'f'},
    {0, 0, 0, 0}
};

void do_cleanup()
{
    static int cleanup_done = 0;

    if(cleanup_done == 0)
    {
        fprintf(stderr, "--> do_cleanup()\n");
        cleanup_done = 1;
        changemode(0);
        close_device();
        x264_close();
        fprintf(stderr,  "\n");
        fprintf(stderr, "<-- do_cleanup()\n");
    }
}

void sigint_handler(int not_used)
{
    do_cleanup();
    signal(SIGINT, SIG_DFL);
    kill(getpid(), SIGINT);
}

int main(int argc, char **argv)
{
    dev_name = "/dev/video0";

    for (;;) {
        int idx;
        int c;

        c = getopt_long(argc, argv, short_options, long_options, &idx);

        if (-1 == c)
            break;

        switch (c) {
        case 0:    /* getopt_long() flag */
            break;

        case 'd':
            dev_name = optarg;
            break;

        case 'h':
            usage(stderr, argc, argv);
            exit(EXIT_SUCCESS);

        case 'o':
            out_buf++;
            break;

        case 'c':
            errno = 0;
            frame_count = strtol(optarg, NULL, 0);
            if (errno) myexit(optarg);
            break;
        case 'm':
            omx_input_port = 90;
            break;
        case 't':
            print_frame_time = 1;
            break;
        case 'f':
            errno = 0;
            frame_rate = strtol(optarg, NULL, 0);
            if (errno) myexit(optarg);
            break;
        default:
            usage(stderr, argc, argv);
            exit(EXIT_FAILURE);
        }
    }

    atexit(do_cleanup);
    signal(SIGINT, sigint_handler);

    open_device();
    init_device();
    x264_open();
    start_capturing();
    mainloop();

    return 0;
}







